package grand.app.moon.presentation.store.viewModels

import grand.app.moon.domain.home.models.Store
import grand.app.moon.domain.story.entity.StoryItem
import grand.app.moon.presentation.base.BaseViewModel

class ItemStoreViewModel  constructor(val store: Store) : BaseViewModel()