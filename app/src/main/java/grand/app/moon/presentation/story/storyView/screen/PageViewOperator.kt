package grand.app.moon.presentation.story.storyView.screen

interface PageViewOperator {
    fun backPageView()
    fun nextPageView()
}