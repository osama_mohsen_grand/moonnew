package grand.app.moon.presentation.base.utils

object Constants {
  const val PAGE_INDEX = 1
  const val NETWORK_PAGE_SIZE = 2

  const val PICK_IMAGE = 1
  const val REGISTER = 2
  const val FORGET_PASSWORD = 3
  const val CONFIRM_CODE = 4
  const val TEACHER_PROFILE = 5
  const val REVIEW_DIALOG = 6
  const val REVIEWS = 7
  const val BACK = 8
  const val CLICK_EVENT = 9
  const val CONTINUE_PROGRESS = 10
  const val CONTACT = 11
  const val COUNTRY = 12
  const val LANG: Int = 13
  const val OPEN_BROWSER: Int = 14
  const val GROUP_DETAILS: Int = 15
  const val FIRST_TIME: Int = 16
  const val HOME: Int = 17
  const val ACTION: Int = 18
  const val LOGIN_REQUIRED: Int = 19


  const val CHAT_APP_ID = "2065015ed5ff1e4d"
  const val CHAT_REGION: String = "us"
  const val CHAT_AUTH_KEY: String = "870588dbabec49c8ccaacfcdc7abc262bd53ad64"
  const val Verify: String = "verify"
  const val IMAGE: String = "image"
  const val COUNTRY_ID = "COUNTRY_ID"
  const val INTRO: String = "INTRO"

  const val TOKEN = "TOKEN"
  const val SPLASH: String = "splash"
  const val SOCIAL_TYPE: String = "social_media"
  const val PICKER_IMAGE: String = "PICKER_IMAGE"
  const val STORIES: String = "stories"
  const val MORE: String = "more"
  const val NEXT: String = "next"
  const val SKIP: String = "skip"
  const val FOLLOW: String = "follow"
  const val LANGUAGE: String = "language"
  const val LOGIN: String = "login"
  const val LOGOUT: String = "logout"
  const val PROFILE: String = "profile"
  const val LAST_ADS: String = "last_ads"
  const val STORES_FOLLOWED: String = "stores_followed"
  const val LAST_SEARCH: String = "last_search"
  const val FAVOURITE: String = "favourite"
  const val STORES_BLOCKED: String = "stores_blocked"


  const val MAIL: String = "mail"
  const val LOCATION: String = "location"
  const val PHONE: String = "phone"


}