package grand.app.moon.presentation.notfication.viewmodel

import grand.app.moon.domain.settings.models.NotificationData
import grand.app.moon.presentation.base.BaseViewModel

class ItemNotificationViewModel  constructor(val model: NotificationData) : BaseViewModel()