package grand.app.moon.presentation.explore.viewmodel

import grand.app.moon.domain.explorer.entity.Explore
import grand.app.moon.presentation.base.BaseViewModel

class ItemExploreViewModel  constructor(val model: Explore) : BaseViewModel()