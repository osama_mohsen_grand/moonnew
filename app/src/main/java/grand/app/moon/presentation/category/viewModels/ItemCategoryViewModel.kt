package grand.app.moon.presentation.category.viewModels

import grand.app.moon.domain.categories.entity.CategoryItem
import grand.app.moon.domain.story.entity.StoryItem
import grand.app.moon.presentation.base.BaseViewModel

class ItemCategoryViewModel  constructor(val category: CategoryItem) : BaseViewModel()