package app.grand.tafwak.presentation.reviews.viewModels

import grand.app.moon.domain.home.models.Reviews
import grand.app.moon.presentation.base.BaseViewModel

class ItemReviewViewModel  constructor(val reviews: Reviews) : BaseViewModel()