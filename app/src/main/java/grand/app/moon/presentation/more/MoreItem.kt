package grand.app.moon.presentation.more

import android.graphics.drawable.Drawable

data class MoreItem(
  var title: String,
  var icon: Drawable,
  var id: Any,
)
