package grand.app.moon.domain.explorer.entity


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import grand.app.moon.domain.home.models.Store
import java.io.Serializable

@Keep
data class Explore(
  val id: Int = 0,
  @SerializedName("created_at")
  val createdAt: String = "",
  @SerializedName("file")
  val file: String = "",
  @SerializedName("mime_type")
  val mimeType: String = "",
  @SerializedName("likes_count")
  val likes: Int = 0,
  @SerializedName("shares_count")
  val shares: Int = 0,
  @SerializedName("comments_count")
  val comments: Int = 0,
  @SerializedName("is_liked")
  val isLike: Boolean = false,
  @SerializedName("store")
  var store: ArrayList<Store> = arrayListOf(),


  ) : Serializable