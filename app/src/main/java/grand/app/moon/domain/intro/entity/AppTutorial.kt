package grand.app.moon.domain.intro.entity

import java.io.Serializable


data class AppTutorial(var title: String, var content: String, var image: String) : Serializable